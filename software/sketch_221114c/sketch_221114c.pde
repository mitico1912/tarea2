import ddf.minim.*;
Minim minim;
AudioPlayer player;

float aceleracion=0; //Variable aceleracion inicializada en 0
float frenado=0;     //Variable frenado inicializada en 0
float velocidad=0;   //Variable velocidad inicializada en 0
float tiempo=0;      //Variable tiempo inicializada en 0


void setup(){
size(1300,1300);                            //Tamaño de la ventana
minim = new Minim(this);                   //Función minim
player = minim.loadFile("perforar_1.mp3");}     //Sonido escogido
  
  
void draw(){
background(51,102,51);  //Color del fondo
fill(255);                 //Color blanco para texto
textSize(80);             //Tamaño de fuente para el sensor de frenado
text("Frenado",100,120);
textSize(80);             //Tamaño fuente para el sensor de aceleración
text("Aceleración",900,120);
textSize(80);             //Tamaño fuente para el sensor de aceleración
text("Velocidad",480,120);


text(frenado,200,190);     //Visualizador del valor de frenado
text(aceleracion,900,190); //Visualizador del valor de la aceleración
text(velocidad,560,190);   //Visualizador del valor de la velocidad

float distancia3=dist(mouseX,mouseY,1000,300); //Variable entera para captar la posición del mouse
if (distancia3<100){
velocidad=velocidad+0.5;                          //Aumento de aceleracionatura gradual
fill(100,20,100);                                //Color del sensor cuando el cursor esta por encima
}
else {velocidad=velocidad-0.1;} //Condición si el mouse se encuentra fuera del área
if (frenado>0){velocidad=velocidad-0.1;
fill(velocidad);            //Visualizador gradual velocidad
fill(200,0,0);
ellipse(680,320,200,200);}  //Luz roja que indica el frenado
if (velocidad<0){velocidad=0;} 

float distancia2=dist(mouseX,mouseY,1000,300);//Variable entera para captar la posición del mouse
fill(0); stroke(100,100,100); strokeWeight(10);    
if (distancia2<100){;
aceleracion=aceleracion+0.1;                     //Aumento de aceleración gradual
fill(200,0,200);                               //Color del sensor cuando el cursor esta sobre el punto de contacto
if (distancia2 < 100){tiempo=tiempo+1;}
if (tiempo==555){player.play(5);}
}
else {aceleracion=aceleracion-0.5;tiempo=0;}     //Condición si el mouse se encuentra fuera del área
if (aceleracion<0){aceleracion=0;}   
if (aceleracion>277.5){aceleracion=277.5;}  
ellipse(1000, 300, 100, 100);    //Sensor de aceleración
float distancia = dist(mouseX,mouseY,300,320);// Variable entera para captar la posicion del mouse en los sensores


fill(255); stroke(100,100,0); strokeWeight(10);
if (distancia<100){
  frenado=frenado+0.5;          //Aumento del frenado gradualmente
fill(200,0,100);}             //Color del sensor cuando el cursor está sobre el punto de contacto
else{frenado=frenado-0.5;}      //Condición si el mouse se encuentra fuera del área
if (frenado<0){frenado=0;}        
if (frenado>277.5){frenado=277.5;}   
if (velocidad==0){frenado=0;}
ellipse(300,320,200,200);     //Sensor de frenado
fill(0,frenado,0);            //Visualizador gradual del sensor de frenado
ellipse(300,550,150,150); 


fill(aceleracion,aceleracion,aceleracion);        //Visualizador gradual sensor de aceleración
ellipse(900,550,150,150); 

fill(velocidad,velocidad,0);        //Visualizador gradual sensor de aceleración
ellipse(600,550,150,150); 

if(frenado>0){               //Texto de alerta
  textSize(50);
  fill(100);
text("STOPPPP",450,450);
}


if(aceleracion>55){
  fill(255,0,0);
  textSize(50);
text("¡¡ACELERACIÓN MÁXIMA ALCANZADA!!",220,200);     //Texto de alerta
text("¡¡ACELERACIÓN MÁXIMA ALCANZADA!!",220,400);
text("¡¡ACELERACIÓN MÁXIMA ALCANZADA!!",220,600);
}
}
